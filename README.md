# Fortune App
## What is it?
Fortune app is a small school assignment where I learned much about the DOM, lists and how to use an Java API. The assignment is based on a Java program called  [Kprog Fortune](https://github.com/yrgohrm/kprog-fortune). 

## Yes, but, what is it really?
Its a website that can give you funny qoutes and showerthoughts right into your web-browser. Join the fun and tell your collegues. 
![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.giphy.com%2Fmedia%2FXreQmk7ETCak0%2Fgiphy.gif&f=1&nofb=1)

## Running:

After unzipping you will have to start [kprog-fortune-1.0-all.jar](https://github.com/yrgohrm/kprog-fortunethe) and follow the instructions [here](https://github.com/yrgohrm/kprog-fortunethe).

Then go to localhost:8080 (if thats the port you chose), and press away at the buttons!

## Enjoy!

![](https://media.giphy.com/media/xUOxf7XfmpxuSode1O/giphy.gif)
